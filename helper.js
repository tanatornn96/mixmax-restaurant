'use strict';

const _ = require('lodash');

const path = require('path');
const Promise = require('bluebird');
const request = Promise.promisifyAll(require('request'));
const fs = Promise.promisifyAll(require('fs-extra'));

const config = require('./config');

const formatData = (result) => {
  let imageUrl;
  if (result.photos) {
    const photoReference = result.photos[0]["photo_reference"];
    imageUrl = `https://maps.googleapis.com/maps/api/place/photo?key=${config.google}&` +
      `photoreference=${photoReference}&maxwidth=${result.photos[0].width}`;
    const filePath = path.join(__dirname, `static/images/cache/${result.id}.jpg`);
    return fs.ensureDirAsync(path.join(__dirname, `static/images/cache/`))
      .then(() => fs.statAsync(filePath))
      .catch(err => (
        new Promise((resolve, reject) => {
          request.get(imageUrl).pipe(fs.createWriteStream(filePath))
            .on('close', () => {
              resolve(true);
             })
            .on('error', (err) => reject(err));
        })
      ))
      .then(() => {
        return {
          id: result.id,
          image: `/images/cache/${result.id}.jpg`,
          name: result.name,
          rating: result.rating,
          address: result.vicinity || result['formatted_address'],
          long: result.geometry.location.lng,
          lat: result.geometry.location.lat,
        }
      })
  } else {
    return {
      id: result.id,
      name: result.name,
      rating: result.rating,
      address: result.vicinity || result['formatted_address'],
      long: result.geometry.location.lng,
      lat: result.geometry.location.lat,
    };
  }

};


const createStars = (rating) => {
  const style = "padding: 0 0.2em; color: white;";

  const stars = _.range(Math.floor(rating)).map((i) => `<i class='fa fa-star' style="${style}"></i>`);
  if (rating - Math.floor(rating) > 0.5) {
    stars.push(`<i clas='fa fa-star-half-o' style="${style}"></i>`);
  }
  while (stars.length < 5) {
    stars.push(`<i class='fa fa-star-o' style="${style}"> </i>`);
  }
  return stars.reduce((prev, curr) => prev += curr, '');
};

const generateMarkup = (restaurant) => {
  return {
    body: `
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <a href="https://www.google.ca/maps/search/${restaurant.name.split(' ').join('+')}/`
      +`@${restaurant.lat},${restaurant.long},15z"
      style="text-decoration:none;">
      <div style="box-shadow: 0 0.1em 0.2em 0 rgba(0,0,0,0.5); margin: 2em; width: 25em; height: 100%;">
        <img src="http://localhost:3000/${restaurant.image}"
          style="object-fit: cover; width: 25em; height: 10em; margin-bottom: -0.25em;
          display: ${restaurant.image ? 'inline-block' : 'none'}" />
        <div style="height: 7em; background: #4285f4;">
          <div style="display: flex; align-items: center;">
            <p style="font-family: lato; color: white; flex: 1; font-size: 1.5em; font-weight: 300;
            margin: 0.5em 0.75em; text-decoration: none">${restaurant.name}</p>
          </div>
          <div style="align-items: center; display: flex; flex-direction: row;">
            <div style="margin-left: 1em;">${createStars(restaurant.rating)}</div>
            <p style="color: white; flex: 1; font-family: lato; font-weight: 300; margin: 0.2em 1em; text-align:right">
              ${restaurant.address}
            </p>
          </div>
        </div>
      </div>
    </a>
    `
  };
}
module.exports = {
  createStars,
  formatData,
  generateMarkup,
};

