const path = require('path');
const webpack = require('webpack');

module.exports = {
  // or devtool: 'eval' to debug issues with compiled output:
  entry: [
    // necessary for hot reloading with IE:
    // listen to code updates emitted by hot middleware:
    'webpack-hot-middleware/client',
    // your code:
    './client/src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/dist/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel'],
      include: path.join(__dirname, 'src')
    }]
  }
};
