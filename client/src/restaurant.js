import React from 'react';

const cardStyle = {
  borderTopLeftRadius: '0.25em',
  borderTopRightRadius: '0.25em',
  borderBottomLeftRadius: '0.25em',
  borderBottomRightRadius: '0.25em',
  boxShadow: '0 0.1em 0.2em 0 rgba(0,0,0,0.50)',
  height: '100%',
  margin: '2em',
  width: '25em',
};

const cardBody = {
  borderBottomLeftRadius: '0.25em',
  borderBottomRightRadius: '0.25em',
  height: 'auto',
  paddingBottom: '1em',
  background: '#4285f4',
};

const imageStyle = {
  borderTopLeftRadius: '0.25em',
  borderTopRightRadius: '0.25em',
  width: '25em',
  height: '10em',
  objectFit: 'cover',
  marginBottom: '-0.25em',
};


const topRow = {
  display: 'flex',
  alignItems: 'center',
};

const restaurantName = {
  color: 'white',
  flex: 1,
  fontFamily: 'lato',
  fontSize: '1.5em',
  fontWeight: 300,
  margin: '0.5em 0.75em',
};

const restaurantAddress = {
  color: 'white',
  flex: 1,
  fontFamily: 'lato',
  fontWeight: 300,
  margin: '0.2em 1em',
  textAlign: 'right',
};

const bottomRow = {
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'row',
};

export default class Restaurant extends React.Component {

  handleOnClick() {
    Mixmax.done(this.props.restaurant);
  }

  createStars(rating) {
    const style = {
      padding: '0 0.2em',
      color: 'white',
    };

    const stars = _.range(Math.floor(rating)).map((i) => <i className='fa fa-star' style={style} key={i}/>);
    if (rating - Math.floor(rating) > 0.5) {
      stars.push(<i className='fa fa-star-half-o' style={style} key={stars.length}/>);
    }
    while (stars.length < 5) {
      stars.push(<i className='fa fa-star-o' style={style} key={stars.length} />);
    }
    return stars;

  }

  render() {
    const { address, name, id, image, rating, lat, long } = this.props.restaurant;
    const stars = this.createStars(rating);
    return image ? (
      <div style={cardStyle}>
        <img style={imageStyle} src={image} />
        <div style={cardBody}>
          <div style={topRow}>
            <p style={restaurantName}>{name}</p>
            <a onClick={this.handleOnClick.bind(this)}>
             <i className="fa fa-share" style={{ marginRight: '1em', color: 'white', cursor: 'pointer'}} />
            </a>
          </div>
          <div style={bottomRow}>
            <div style={{ marginLeft: '1em' }}>{rating ? stars : '' }</div>
           <p style={restaurantAddress}>{address}</p>
          </div>
        </div>
      </div>
    ) : (
       <div style={cardStyle}>
        <div style={cardBody}>
          <div style={topRow}>
            <p style={restaurantName}>{name}</p>
            <a onClick={this.handleOnClick.bind(this)}>
             <i className="fa fa-share" style={{ marginRight: '1em', color: 'white', cursor: 'pointer'}} />
            </a>
          </div>
          <div style={bottomRow}>
            <div style={{ marginLeft: '1em' }}>{rating ? stars : '' }</div>
           <p style={restaurantAddress}>{address}</p>
          </div>
        </div>
      </div>

    );
  }

}

