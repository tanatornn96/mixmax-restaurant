import React from 'react';
import { render } from 'react-dom';
import Promise from 'bluebird';
import 'whatwg-fetch';
import _ from 'lodash';

import Restaurant from './restaurant';

const navigationNotSupported = !navigator.geolocation;
const getCurrentPosition = () => (
  new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(data => resolve(data), () => reject(-1));
}));

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      restaurants: [],
    };
  }


  getLocation() {
    this.setState(Object.assign(this.state, { isLoading: true }));
    return getCurrentPosition().bind(this)
      .then(({ coords }) => (
        fetch(`/search?longitude=${coords.longitude}` +
          `&latitude=${coords.latitude}`, { mode: 'cors' })
      ))
      .then((response) => response.json())
      .then((json) => {
        this.setState({
          restaurants: json,
          isLoading: false,
        });
      });
  }

  getRestaurantList() {
   return this.state.restaurants.map((restaurant, index) => {
      return <Restaurant key={index} restaurant={restaurant}/>
    });
  }

  handleOnClick(params) {
    Mixmax.done(params);
  }

  handleKeyUp(e) {
    if (e.keyCode === 13) {
      this.setState(Object.assign(this.state, { isLoading: true }));
      fetch(`/search?query=${e.target.value}`, { mode: 'cors' })
        .then(res => res.json())
        .then(restaurants => this.setState({
          isLoading: false,
          restaurants
        }));
    }
  }

  render() {

    const restaurants = this.getRestaurantList();
    const titleStyle = { fontFamily: 'lato',
      fontWeight: 200,
      fontSize: '2em',
      color: 'white',
    };

    const titleContainerStyle = {
      height: '3.4em',
      width: '100%',
      background: 'rgb(66, 133, 244)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    };

    const questionContainer = {
       display: 'flex',
       alignItems: 'center',
       justifyContent: 'center',
       margin: '1em',
       flexDirection:'column',
    };

    const questionStyle = {
      fontFamily: 'lato',
      fontSize: '1.5em',
      fontWeight: '300',
    };

    const inputStyle = {
      boxShadow: '0 0.1em 0.2em 0 rgba(0,0,0,0.50)',
      width: '13em',
      padding: '0.25em',
      borderRadius: '0.25em',
      outline: 'none',
      border: 'none',
      fontSize: '1.2em',
      textAlign: 'center',
    };

    const pointer = { cursor: 'pointer',
      margin: '1em',
    };

    const restaurantContainer = {
      background: '#f1f1f1',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
      width: '100%'
    };

    return (
      <div style={{ background: '#f1f1f1', height: '100%' }}>
        <div style={titleContainerStyle}>
          <span style={titleStyle}>Restaurant</span>
        </div>
        <div style={questionContainer}>
          <p style={questionStyle}>What is the restaurant you're looking for?</p>
          <div>
            <input style={inputStyle} onKeyUp={this.handleKeyUp.bind(this)}/>
            <a onClick={this.getLocation.bind(this)} className="fa fa-location-arrow" style={pointer}></a>
            <i className="fa fa-spinner fa-spin"
              style={{ display: this.state.isLoading ? 'inline-block' : 'none' }}/>
          </div>
        </div>
        <div style={restaurantContainer}>
          {restaurants}
        </div>
      </div>
    );
  }
}

render(<App />, document.getElementById('root'));

