# Mixmax Restaurant Enhancement

> A Mixmax add-on that allows you to search a nearby restaurant send it to a friend

## Setup

1. Clone the repository
2. `npm install`
3. `npm start`
4. As for adding the enhancement the defaults are:
```
Name : Restaurant
Icon Tooltip : Find a restaurant!
Editor URL : http://localhost:3000
Resolver API URL : http://localhost:3000/api/resolver
```
5. Since this application uses Google API, you must provide a valid Google API key with `Google Places API Web Service` enabled. we put this in a `config.js` in the base folder like so,

```
module.exports = {
  google: <YOUR API KEY>
};
```
