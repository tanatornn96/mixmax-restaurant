'use strict';

const path = require('path');
const Promise = require('bluebird');
const request = Promise.promisifyAll(require('request'));
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const config = require('./config');
const helper = require('./helper');

const app = express();

app.use(express.static('static/'));
app.use(bodyParser.urlencoded({ extended: true }));

const catchError = (err) => console.log(err);

app.get('/search', (req, res) => {
  let queryUrl;
  if (req.query.latitude && req.query.longitude) {
    const coordinates = `${req.query.latitude},${req.query.longitude}`;
    queryUrl = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=${config.google}&` +
      `location=${coordinates}&radius=10000&type=restaurant`;
  } else {
    queryUrl = `https://maps.googleapis.com/maps/api/place/textsearch/json?key=${config.google}&` +
      `query=${req.query.query}&radius=10000`;
  }

  request.getAsync(queryUrl)
     .then(response => {
       const body = JSON.parse(response.body);
       return Promise.all(body.results.map(helper.formatData));
     })
     .then(data => res.send(data))
     .catch(catchError);
});

const corsOptions = {
  origin: /^([^.\s]+\.mixmax\.com)$/,
  credentials: true
};

app.post('/api/resolver', cors(corsOptions), (req, res) => {
  const restaurant = JSON.parse(req.body.params);
  res.json(helper.generateMarkup(restaurant));
});

app.listen(3000);

